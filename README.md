# Nutshell

Nutshell is an open-source Customer Relationship Management (CRM) application. 

# About Nutshell

Nutshell is a Customer Relationship Manager (CRM) designed for businesses about 1 to 100 collaborators.

Customer relationship management (CRM) is an approach to managing a company's interaction with current and potential customers. It uses data analysis about customers' history with a company and to improve business relationships with customers, specifically focusing on customer retention and ultimately driving sales growth.


# Functionality:

- Customer Management
- Dedicated Customer Profile
- Invoices
- Reports and Dashboards
- Meetings


# Main Contributors

- [EHIDIAMEN IMADEGBOR (Lead)] (https://gitlab.com/imadehidiame)
- [Joye Shonubi] (https://gitlab.com/Oladunjoye)
- [Pamela Agbakoba] (https://gitlab.com/desirable1)
- [Naimat Oyewale] (https://gitlab.com/Naimahtech)
- [Nathanael Bakare] (https://gitlab.com/nathyjegs)
